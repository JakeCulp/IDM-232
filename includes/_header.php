<html>
<head>
	<title>Drexel Powerlifting</title>
	<link rel="stylesheet" href="style.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0, width=device-width">
	<link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link href="animate.css" rel="stylesheet">
	<link href="http://cdn.rawgit.com/noelboss/featherlight/1.7.6/release/featherlight.min.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="grid">
	<header>
		<div class="logo"><a href="aboutus.php"><img src="dupl_outline.svg" alt="drexel powerlifting logo"></img></a></div>
			<div class="links">
				<a href="photos.php">Photos</a>
				<a href="aboutus.php">About Us</a>
				<a href="lifters.php">Lifters</a>
				<a href="contact.php">Contact</a>
			</div>
</header>
